/**
 * Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Zebadua Hinrichs
 * Blatt 07, Abgabe: 02.12.2019
 * @author Lennart Ferlings
 */
package gruppe01.info07;

import java.util.Stack;

public class PushdownAutomaton {
	// Member Variablen
	private int              mState;     // aktueller Zustand des endlichen Automaten
	private Stack<Character> mStack;     // Keller
	private int[]            mEndStates; // Liste mit allen Endzustaenden
	
	// Der Konstruktor wird beim Erstellen einer Instanz dieser Klasse aufgerufen. 
	// Dabei werden alle Member Variablen dieser Klasse initialisiert. So starten
	// wir im Zustand 0, erzeugen einen leeren Keller und fuegen alle Endzustaende
	// (hier nur der Endzustand 3) zu unserer Liste von Endzustaenden.
	public PushdownAutomaton() {
		// wir starten im ersten Zustand
		mState = 0;
		
		// initialisiere den Keller
		mStack = new Stack<Character>();
		
		// initialisiere Endzustaende mit dem Zustand 3
		mEndStates = new int[]{3};
	}
	
	// main ist der Startpunkt unseres Programms
	public static void main(String args[]) {
		PushdownAutomaton pda = new PushdownAutomaton();
		
		// Eingabe 1
		String input = "abaa";
		System.out.println("Akzeptiert der Kellerautomat die Eingabe " + input + "?");
		boolean status = pda.accept(input);
		System.out.println("Erwarte: true");
		System.out.println("Bekomme: " + status + "\n");
		
		// Eingabe 2
		input = "aba";
		System.out.println("Akzeptiert der Kellerautomat die Eingabe " + input + "?");
		status = pda.accept(input);
		System.out.println("Erwarte: false");
		System.out.println("Bekomme: " + status + "\n");
		
		// Eingabe 3
		input = "aaabaaaaaa";
		System.out.println("Akzeptiert der Kellerautomat die Eingabe " + input + "?");
		status = pda.accept(input);
		System.out.println("Erwarte: true");
		System.out.println("Bekomme: " + status + "\n");
	}
	
	// accept nimmt einen String bestehend aus den Buchstaben 'a' und 'b'
	// und gibt true zurueck wenn die Eingabe die Form a^n b^m a^(2n) hat
	// und false andernfalls. Dabei muessen n,m groesser als 0 sein.
	public boolean accept(String input) {
		// Setze den Zustand des Kellerautomaten auf den
		// Startzustand zurueck
		mState = 0;
		mStack.clear();
		
		// iteriere ueber alle Buchstaben der Eingabe
		for(int i = 0; i < input.length(); i++) {
			// nehme den naechsten Buchstaben aus der Eingabe 
			char character = input.charAt(i);
			
			// lese das aktuelle oberste Element des Kellers
			char top;
			if(mStack.empty()) top = '#';
			else               top = mStack.peek();
			
			// gebe den aktuellen Zustand des Kellerautomates aus
			printAutomatonState(character, top, mState, mStack);
			
			// fuere Keller Operation basieren auf der aktuellen
			// Eingabe, oberstem Keller Element und Zustand aus
			stackAction(character, top, mState, mStack);
			
			// wechsele in den naechsten Zustand mithilfe der
			// Zustandsaenderungsfunktion
			mState = nextState(character, top, mState);
		}
		
		// gebe den finalen Zustand des Kellerautomates aus
		char top;
		if(mStack.empty()) top = '#';
		else               top = mStack.peek();
		printAutomatonState(' ', top, mState, mStack);
		
		// pruefe ob Eingabe akzeptiert wird.
		return checkIfInputIsAccepted(mState, mEndStates, mStack);
	}
	
	// printAutomatonState gibt den aktuellen Zustand des Kellerautomaten auf der Konsole aus.
	private void printAutomatonState(char character, char top, int state, Stack<Character> stack) {
		// erzeuge Visualisierung des Kellers
		String stackVis = "";
		for (int k = 0; k < mStack.size(); k++) 
			stackVis += mStack.get(k);
		stackVis += "#";
		
		// gebe aktuellen Zustand des Automaten auf der Konsole aus.
		System.out.println(character + " " + top + " " + state + " - " + stackVis);
	}
	
	// stackAction fuer eine Kelleroperation basierend auf dem gelesenen Zeichen,
	// dem obersten Element auf dem Keller und dem aktuellen Zustand des endlichen
	// Zustandautomaten aus.
	private void stackAction(char character, char top, int state, Stack<Character> stack) {
		// --------------------------------------------------------------------- //
		// Aufgabe b) //
		// Implementieren Sie die Keller-Operationen basierend auf der Eingabe //
		// bestehend aus character, top und state. Wobei character das aktuell //
		// gelesene Zeichen ist, top ist das aktuelle gelesene Zeichen oben auf //
		// dem Keller oder der leere Keller '#' und der aktuelle Zustand des //
		// endlichen Zustandsautomaten. //
		// --------------------------------------------------------------------- //

		// Es wird nur bei einem 'a' eine Operation auf den Stack ausgef�hrt
		if (character == 'a') {
			if (state == 0)
				stack.push('A'); // Egal ob '#' oder 'A'
			else if (state == 2)
				stack.pop(); // L�schen vom obersten Element
		}

	}
	
	// nextState modellierte die Zugangsuebergangsfunktion, die das aktuell gelesene
	// Zeichen, das oberste Element auf dem Keller und den aktuellen Zustand des
	// endlichen Zustandsautomaten nimmt um den neuen Zustand zu bestimmen.
	private int nextState(char character, char top, int state) {
		// --------------------------------------------------------------------- //
		// Aufgabe c) //
		// Implementieren Sie die Zugangsuebergangsfunktion entsprechend dem //
		// Zustandsdiagramm des Uebungsblatts und basierend auf der Eingabe //
		// bestehend aus character, top und state. Wobei character das aktuell //
		// gelesene Zeichen ist, top ist das aktuelle gelesene Zeichen oben auf //
		// dem Keller oder der leere Keller '#' und der aktuelle Zustand des //
		// endlichen Zustandsautomaten. //
		// Bedenken Sie, dass falsche Kombinationen von character, top und state //
		// in einem Fehler-Zustand enden sollten. //
		// --------------------------------------------------------------------- //

		if (state == 0) {
			if (character == 'a')
				return 0;
			else if (character == 'b' && top == 'A')
				return 1;

		} else if (state == 1) {
			if (character == 'a')
				return 2;
			else if (character == 'b')
				return 1;

		} else if (character == 'a' && top == 'A') {
			if (state == 2)
				return 3;
			else if (state == 3 && top == 'A')
				return 2;
		}

		return -1;
	}
	
	// checkIfInputIsAccepted prueft ob der Automat die Eingabe akzeptiert.
	// Dabei wird eine Eingabe akzeptiert, wenn ein Endzustand erreicht wurde
	// UND der Keller leer ist.
	private boolean checkIfInputIsAccepted(int state, int[] endStates, Stack<Character> stack) {
		// --------------------------------------------------------------------- //
		// Aufgabe a) //
		// Implementieren Sie eine Funktion, die ueberprueft ob die Eingabe //
		// akzeptiert wird. Dabei akzeptiert ein Keller-Automat eine Eingabe nur //
		// dann wenn ein Endzustand erreicht wurde und der Keller leer ist. //
		// --------------------------------------------------------------------- //

		if (stack.empty() || stack.peek() == '#') {
			for (int endState : endStates) {
				if (state == endState)
					return true;
			}

		}
		return false;
	}
}
